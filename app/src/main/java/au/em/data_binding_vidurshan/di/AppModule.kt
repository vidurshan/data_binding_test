package au.em.testaugustvidurshan.di

import au.em.data_binding_vidurshan.api.HotelsApi
import au.em.testaugustvidurshan.uitls.Constants.BASE_URL
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object AppModule {
    @Provides
    fun provideBaseUrl() = BASE_URL

    @Provides
    @Singleton
   fun provideRetrofitInstance(base_url: String):HotelsApi =
       Retrofit.Builder()
           .baseUrl(base_url)
           .addConverterFactory(GsonConverterFactory.create())
           .build()
           .create(HotelsApi::class.java)


}