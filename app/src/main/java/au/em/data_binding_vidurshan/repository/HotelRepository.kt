package au.em.testaugustvidurshan.repository

import au.em.data_binding_vidurshan.api.HotelsApi
import javax.inject.Inject

class HotelRepository
@Inject constructor(private val hotelsApi: HotelsApi) {
    suspend fun getAlllHotels()  =hotelsApi.getAllHotels()
}