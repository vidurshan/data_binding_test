package au.em.testaugustvidurshan.viewmodel

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import au.em.testaugustvidurshan.model.Hotel
import au.em.testaugustvidurshan.repository.HotelRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class HotelsViewModel
@Inject constructor(private val repository: HotelRepository) : ViewModel() {
    private val getHotelLiveData = MutableLiveData<Hotel>()

    val responseHotels: LiveData<Hotel> get() = getHotelLiveData

    init {
        getAllHotels()
    }

    private fun getAllHotels() = viewModelScope.launch {
        repository.getAlllHotels().let { response ->
            if (response.isSuccessful) {
                getHotelLiveData.postValue(response.body())
            } else {
                Log.d("Hotel", "Get All Hotel Error : ${response.errorBody()}")
            }
        }
    }
}