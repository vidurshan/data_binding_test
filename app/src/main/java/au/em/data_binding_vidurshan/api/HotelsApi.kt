package au.em.data_binding_vidurshan.api

import au.em.testaugustvidurshan.model.Hotel
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Headers
import au.em.testaugustvidurshan.uitls.Constants.HOTELS_END_POINT


interface HotelsApi
{
    @Headers("Content-Type:application/json")
    @GET(HOTELS_END_POINT)
    suspend fun getAllHotels(): Response<Hotel>
}