package au.em.data_binding_vidurshan.ui

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.activity.viewModels
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.LinearLayoutManager
import au.em.data_binding_vidurshan.R
import au.em.data_binding_vidurshan.databinding.ActivityMainBinding
import au.em.testaugustvidurshan.ui.adapter.HotelsAdapter
import au.em.testaugustvidurshan.viewmodel.HotelsViewModel
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class MainActivity : AppCompatActivity() {



    lateinit var hotelsAdapter: HotelsAdapter
    private val vm: HotelsViewModel by viewModels()
    override fun onCreate(savedInstanceState: Bundle?) {

        super.onCreate(savedInstanceState)
        val binding: ActivityMainBinding = DataBindingUtil.setContentView(
            this, R.layout.activity_main)

        hotelsAdapter = HotelsAdapter(mutableListOf())
        binding.recyclerView.apply {
            adapter = hotelsAdapter
            layoutManager = LinearLayoutManager(this@MainActivity)

        }
        vm.responseHotels.observe(this, { response ->
            hotelsAdapter.hotelList.addAll(response.data)
            hotelsAdapter.notifyDataSetChanged()
        })

    }
}