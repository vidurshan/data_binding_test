package au.em.testaugustvidurshan.ui.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.AsyncListDiffer
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import au.em.data_binding_vidurshan.databinding.ItemHotelBinding

import au.em.testaugustvidurshan.model.Data


class HotelsAdapter(var hotelList: MutableList<Data>): RecyclerView.Adapter<HotelsAdapter.ViewHolder>() {
    inner class ViewHolder(
        private val binding: ItemHotelBinding
    ) : RecyclerView.ViewHolder(binding.root) {

        fun onBind(hotel: Data) {
            binding.hotelData = hotel
            binding.executePendingBindings()
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            ItemHotelBinding.inflate(
                LayoutInflater.from(parent.context),parent,false
            )
        )
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val hotel = hotelList[position]

        holder.onBind(hotel)
    }

    override fun getItemCount(): Int = hotelList.size
}